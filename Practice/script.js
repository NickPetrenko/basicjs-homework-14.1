document.addEventListener('DOMContentLoaded', () => {
    const themeToggleBtn = document.getElementById('toggle-theme');
    const body = document.body;
    const currentTheme = localStorage.getItem('theme') || 'theme-light';
  
    // Set the initial theme
    body.className = currentTheme;
  
    // Toggle theme on button click
    themeToggleBtn.addEventListener('click', () => {
      const newTheme = body.className === 'theme-light' ? 'theme-dark' : 'theme-light';
      body.className = newTheme;
      localStorage.setItem('theme', newTheme);
    });
});
  